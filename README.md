# Example of how to use setuptools

To build changes `python setup.py sdist --formats=gztar,zip`

get newest versions of setuptools and wheel

```bash
python3 -m pip install --user --upgrade setuptools wheel
mkdir example_pkg
cd example_pkg
touch __init__.py
cd ..
touch README.md
touch LICENSE
touch setup.py
```

paste this into setup.py

```python
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="example-pkg-YOUR-USERNAME-HERE", # Replace with your own username
    version="0.0.1",
    author="Example Author",
    author_email="author@example.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.7',
)
```

8) create file example_pk/example.py and input `print hello world"`

9) python3 setup.py sdist bdist_wheel

-created build folder which seems to contain my package
-a dist filer with two files
 -example_pkg_YOUR_USERNAME_HERE-0.0.1-py3-none-any.whl
 -example_pkg_YOUR_USERNAME_HERE-0.0.1.tar.gz
-created directory named example_pkg_YOUR_USERNAME_HERE.egg-info. This contains four files.

